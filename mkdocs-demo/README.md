以 Mac OS + Python 3.7.3 为例

1. 创建虚拟环境: `python -m venv venv`
2. 启动虚拟环境: `source venv/bin/activate`
3. 安装python包: `pip install -r requirements.txt -i https://pypi.douban.com/simple/`
4. 启动Mkdocs: `mkdocs serve`
5. 退出虚拟环境: `deactivate`